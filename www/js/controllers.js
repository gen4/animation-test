angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});


})

.controller('NoCtrl', function($scope,$timeout,Const) {
  $scope.scales = Const.scales;
  $scope.currVar = 0;
  $scope.circles = new Array($scope.scales[$scope.currVar].q);

  $scope.doubleCircles  =  function(){
      $scope.currVar++;
      if($scope.currVar>=$scope.scales.length){
        $scope.currVar=0
      }
      $scope.circles = new Array($scope.scales[$scope.currVar].q);
  }
})

.controller('CssCtrl', function($scope,$timeout,Const) {
  $scope.scales = Const.scales;

  $scope.currVar = 0;
  $scope.circles = new Array($scope.scales[$scope.currVar].q);

  var interval = null

  $scope.doubleCircles  =  function(){
    if(interval){
      $timeout.cancel(interval)
    }
    $scope.class='fadeout';
     interval = $timeout(function(){
      $scope.currVar++;
      if($scope.currVar>=$scope.scales.length){
        $scope.currVar=0
      }
      $scope.class='fadein';
      $scope.circles = new Array($scope.scales[$scope.currVar].q);
    },500)
  }
})


    .controller('JsCtrl', function($scope,$timeout,Const) {
      $scope.scales = Const.scales;

      $scope.currVar = 0;
      $scope.circles = new Array($scope.scales[$scope.currVar].q);


      var fadeIn1 = {
        opacity: 1,
        scaleX:1.1,
        scaleY:1.1
      };
      var fadeIn2 = {
        opacity: 0,
        scaleX:0,
        scaleY:0
      };
      var fadeIn3 = {
        opacity: 1,
        scaleX:1,
        scaleY:1
      };

      var interval = null;
      $timeout(function() {
        $('.jsanim')
            .velocity(fadeIn2, {duration: 1})
            .velocity(fadeIn1, {easing: "easeIn", duration: 250})
            .velocity(fadeIn3, {easing: "easeOut", duration: 250});
      },2);

      $scope.doubleCircles  =  function(){
        if(interval){
          $timeout.cancel(interval)
        }
        //$('.jsanim').velocity("finish");
        $('.jsanim')
            .velocity(fadeIn3, { duration: 1 })
            .velocity(fadeIn1, {easing:"easeIn", duration: 250 })
            .velocity(fadeIn2,  {easing:"easeOut",  duration: 250 });
        interval = $timeout(function(){
          $scope.currVar++;
          if($scope.currVar>=$scope.scales.length){
            $scope.currVar=0
          }
          $scope.circles = new Array($scope.scales[$scope.currVar].q);
          $timeout(function() {
            //$('.jsanim').velocity("finish");
            $('.jsanim')
                .velocity(fadeIn2, {duration: 1})
                .velocity(fadeIn1, {easing: "easeIn", duration: 250})
                .velocity(fadeIn3, {easing: "easeOut", duration: 250});
          },2);
        },500)
      }
    })

    .controller('SVGCtrl', function($scope,$timeout,Const) {
      $scope.scales = Const.scales;
      $scope.currVar = 0;
      $scope.circles = new Array($scope.scales[$scope.currVar].q);

      $scope.doubleCircles  =  function(){
        $scope.currVar++;
        if($scope.currVar>=$scope.scales.length){
          $scope.currVar=0
        }
        $scope.circles = new Array($scope.scales[$scope.currVar].q);
      }
    })

.service('Const', function() {
  return {
    scales:[
        {size:200, q:1 },
        {size:100, q:4 },
        {size:100, q:8 },
        {size:40, q:16 },
        {size:40, q:64 },
        {size:20, q:128 },
        {size:10, q:512 },
        {size:8, q:1024 }
      ]
  }
})
    .directive('svgCircle', function(){
      return {
        restrict: 'E',
        scale:{id:'&'},
        templateUrl: '/js/svgcircle.html'
      };
    })
;
