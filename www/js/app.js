// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

      .state('app.no', {
        url: '/no',
        views: {
          'menuContent': {
            templateUrl: 'templates/no.html',
            controller: 'NoCtrl'
          }
        }
      })
  .state('app.css', {
    url: '/css',
    views: {
      'menuContent': {
        templateUrl: 'templates/css.html',
        controller: 'CssCtrl'
      }
    }
  })

  .state('app.svg', {
      url: '/svg',
      views: {
        'menuContent': {
          templateUrl: 'templates/svg.html',
          controller: 'SVGCtrl'
        }
      }
    })
    .state('app.js', {
      url: '/js',
      views: {
        'menuContent': {
          templateUrl: 'templates/js.html',
          controller: 'JsCtrl'
        }
      }
    })


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/no');
});
